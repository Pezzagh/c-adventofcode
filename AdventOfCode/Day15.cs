﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace AdventOfCode
{
    public interface ICookieLoader {
        void AddIngredient(string ingredientText);
        int TestCookie(Dictionary<string, int> formualation);
    }

    public class Ingredient {
        public string Name { get; set; }
        public int Capacity { get; set; }
        public int Durability { get; set; }
        public int Flavour { get; set; }
        public int Texture { get; set; }
        public int Calories { get; set; }
    }

    public class CookieLoader : ICookieLoader
    {
        private Regex _ingredientParser = new Regex(@"(\w+): capacity (-*\d+), durability (-*\d+), flavor (-*\d+), texture (-*\d+), calories (-*\d+)");

        private Dictionary<string, Ingredient> _ingredientList = new Dictionary<string, Ingredient>();

        public void AddIngredient(string ingredientText)
        {
            var match = _ingredientParser.Match(ingredientText);

            _ingredientList.Add(match.Groups[1].Value, new Ingredient{
                Capacity = Convert.ToInt32(match.Groups[2].Value),
                Durability = Convert.ToInt32(match.Groups[3].Value),
                Flavour = Convert.ToInt32(match.Groups[4].Value),
                Texture = Convert.ToInt32(match.Groups[5].Value),
                Calories = Convert.ToInt32(match.Groups[6].Value)
            });
        }

        public int TestCookie(Dictionary<string, int> formualation)
        {
            var output = new Ingredient();

            foreach (var component in formualation) {
                output.Capacity += _ingredientList[component.Key].Capacity * component.Value;
                output.Durability += _ingredientList[component.Key].Durability * component.Value;
                output.Flavour += _ingredientList[component.Key].Flavour * component.Value;
                output.Texture += _ingredientList[component.Key].Texture * component.Value;
                output.Calories += _ingredientList[component.Key].Calories * component.Value;
            }


            return Math.Max(0, output.Capacity) * Math.Max(0, output.Durability) * Math.Max(0, output.Flavour) * Math.Max(0, output.Texture);
        }
    }
}
