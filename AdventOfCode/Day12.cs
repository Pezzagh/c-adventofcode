﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AdventOfCode
{
    public interface IObjectAggregator
    {
        int Aggregate(string json);
    }

    public class ObjectAggregator : IObjectAggregator
    {
        public int ArrayTrawler(JArray obj)
        {
            var tot = 0;
            bool foundRed = false;

            foreach (var prop in obj)
            {
                if (prop.Type == JTokenType.Integer)
                {
                    tot += prop.ToObject<int>();
                    continue;
                }

                if (prop.Type == JTokenType.String)
                {
                    //if (prop.ToObject<string>() == "red")
                    //{
                    //    foundRed = true;
                    //}
                    continue;
                }

                if (prop.Type == JTokenType.Array)
                {
                    tot +=  ArrayTrawler(prop.ToObject<JArray>());
                    continue;
                }

                if (prop.Type == JTokenType.Object)
                {
                    tot +=  ObjectTrawler(prop.ToObject<JObject>());
                    continue;
                }
            }

            return foundRed ? 0 : tot;
        }

        public int ObjectTrawler(JObject obj) {
            var tot = 0;
            bool foundRed = false;

            foreach (var prop in obj)
            {
                string name = prop.Key;
                JToken value = prop.Value;

                if (value.Type == JTokenType.Integer)
                {
                    tot += value.ToObject<int>();
                    continue;
                }

                if (value.Type == JTokenType.String)
                {
                    if (value.ToObject<string>() == "red") {
                        foundRed = true;
                    }
                    continue;
                }

                if (value.Type == JTokenType.Array)
                {
                    tot += ArrayTrawler(value.ToObject<JArray>());
                    continue;
                }

                if (value.Type == JTokenType.Object)
                {
                    tot +=  ObjectTrawler(value.ToObject<JObject>());
                    continue;
                }
            }

            return foundRed ? 0 : tot;
        }

        public int Aggregate(string json)
        {
            var obj = JsonConvert.DeserializeObject<object>(json);

            var jobject = obj as JObject; 
            if (jobject != null) {
                return ObjectTrawler(jobject);
            }

            var jarray = obj as JArray;
            if (jarray != null)
            {
                return ArrayTrawler(jarray);
            }

            return 0;
        }
    }
}
