﻿using System;
using System.Text.RegularExpressions;

namespace AdventOfCode
{
    public class Instruction {
        public Action Action { get; set; }
        public Point TopLeft { get; set; }
        public Point BottomRight { get; set; }
    }

    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
    }

    public enum Action
    {
        On,
        Off,
        Toggle

    }

    public static class StringExtensions
    {
        public static Action GetAction(this string val)
        {
            Action action = Action.On;
            switch (val)
            {
                case "turn on":
                    action = Action.On;
                    break;
                case "turn off":
                    action = Action.Off;
                    break;
                case "toggle":
                    action = Action.Toggle;
                    break;
            }

            return action;
        }

        public static Wiring GetWiring(this string val)
        {
            Wiring wiring = Wiring.Signal;

            switch (val)
            {
                case "":
                    wiring = Wiring.Signal;
                    break;
                case "AND":
                    wiring = Wiring.And;
                    break;
                case "LSHIFT":
                    wiring = Wiring.LShift;
                    break;
                case "RSHIFT":
                    wiring = Wiring.RShift;
                    break;
                case "NOT":
                    wiring = Wiring.Not;
                    break;
                case "OR":
                    wiring = Wiring.Or;
                    break;
            }

            return wiring;
        }
    }

    public interface IInstructionParser
    {
        Instruction ParseText(string instructionText);
    }

    public class InstructionParser : IInstructionParser
    {
        private readonly Regex lineParser = new Regex(@"(turn on|turn off|toggle) (\d+),(\d+) through (\d+),(\d+)");

        public Instruction ParseText(string instructionText)
        {
            var match = lineParser.Match(instructionText);

            return new Instruction
            {
                Action = match.Groups[1].Value.GetAction(),
                TopLeft = new Point
                {
                    X = Convert.ToInt32(match.Groups[2].Value),
                    Y = Convert.ToInt32(match.Groups[3].Value),
                },
                BottomRight = new Point
                {
                    X = Convert.ToInt32(match.Groups[4].Value),
                    Y = Convert.ToInt32(match.Groups[5].Value),
                }
            };
        }
    }

    public interface ILightSwitcher
    {
        void SwitchLights(bool[,] lights, Action action, Point topLeft, Point bottomRight);

        void PhaseLights(int[,] lights, Action action, Point topLeft, Point bottomRight);

        int LumenCount(int[,] lights);

        int CountLights(bool[,] lights);
    }

    public class LightSwitcher : ILightSwitcher
    {
        public int CountLights(bool[,] lights)
        {
            var retval = 0;
            for (var x = 0; x < lights.GetLength(0); x = x + 1)
            {
                for (var y = 0; y < lights.GetLength(1); y = y + 1)
                {
                    if (lights[x, y]) {
                        retval = retval + 1;
                    }
                }
            }
            return retval;
        }

        public int LumenCount(int[,] lights)
        {
            var retval = 0;
            for (var x = 0; x < lights.GetLength(0); x = x + 1)
            {
                for (var y = 0; y < lights.GetLength(1); y = y + 1)
                {
                    retval += lights[x, y];
                }
            }
            return retval;
        }

        public void PhaseLights(int[,] lights, Action action, Point topLeft, Point bottomRight)
        {
            for (var x = topLeft.X; x <= bottomRight.X; x = x + 1)
            {
                for (var y = topLeft.Y; y <= bottomRight.Y; y = y + 1)
                {
                    switch (action)
                    {
                        case Action.On:
                            lights[x, y] += 1;
                            break;
                        case Action.Off:
                            if (lights[x, y] > 0) {
                                lights[x, y] -= 1;
                            }
                            break;
                        case Action.Toggle:
                            lights[x, y] += 2;
                            break;
                    }
                }
            }
        }

        // going to assume points specified wont bust the boundaries of the array.
        public void SwitchLights(bool[,] lights, Action action, Point topLeft, Point bottomRight)
        {
            for (var x = topLeft.X; x <= bottomRight.X; x = x + 1)
            {
                for (var y = topLeft.Y; y <= bottomRight.Y; y = y + 1)
                {
                    switch (action) {
                        case Action.On:
                            lights[x, y] = true;
                            break;
                        case Action.Off:
                            lights[x, y] = false;
                            break;
                        case Action.Toggle:
                            lights[x, y] = !lights[x, y];
                            break;
                    }
                }
            }
        }
    }
}
