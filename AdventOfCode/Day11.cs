﻿using System;
using System.Collections.Generic;

namespace AdventOfCode
{
    public interface IPasswordIncrementer {
        string Increment(string password);
    }

    public class PasswordIncrementor : IPasswordIncrementer
    {
        private string letters = "abcdefghijklmnopquvrstwxyz";


        private string TwiddleChar(string password, int index) {

            if (index < 0) {
                return "a" + password;
            }

            var begin = password.Substring(0,  index);
            var bit = password[index];

            var end = string.Empty;

            if (index + 1 < password.Length) {
                end = password.Substring(index + 1, password.Length - index - 1);
            }

            if (bit != 'z')
            {
                return begin + letters[letters.IndexOf(bit)+1] + end;
            }
            else
            {
                password = TwiddleChar(begin + "a" + end, index - 1);
            }

            return password;
        }

        public string Increment(string password)
        {
            return TwiddleChar(password, password.Length - 1); ;
        }
    }


    public interface IPasswordChecker
    {
        bool ValidPassword(string password);
    }

    public class PasswordChecker : IPasswordChecker
    {
        private IStringSplitter _splitter;

        private string letters = "abcdefghijklmnopquvrstwxyz";

        public PasswordChecker(IStringSplitter splitter) {
            _splitter = splitter;
        }

        public bool ValidPassword(string password)
        {
            if (password.Contains("i") || password.Contains("o") || password.Contains("l")) {
                return false;
            }

            var foundStraight = false;
            foreach (var s in _splitter.GetTokens(password, 3, false)) {
                if (letters.Contains(s)) {
                    foundStraight = true;
                    break;
                }
            }

            if (!foundStraight) {
                return false;
            }

            var matchingPairs = new List<string>();
            foreach (var s in _splitter.GetTokens(password, 2, true))
            {
                if (s[0] == s[1]) {
                    if (!matchingPairs.Contains(s)) {
                        matchingPairs.Add(s);
                    }
                }
            }

            return matchingPairs.Count > 1;
        }
    }
}
