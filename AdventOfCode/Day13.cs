﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode
{
    public interface IDinnerLoader {
        void AddDinerEntries(List<string> entries);
        int GetModifier(int a, int b);
        int GetModifier(string a, string b);
    }

    public class DinnerLoader : IDinnerLoader
    {
        private Regex _dinnerParser = new Regex(@"(\w+) would (\w+) (\d+) happiness units by sitting next to (\w+).");
        private List<string> _diners = new List<string>();
        private Dictionary<Tuple<int, int>, int> _modifiers = new Dictionary<Tuple<int, int>, int>();

        public void AddDinerEntries(List<string> entries)
        {
            foreach (var entry in entries) {
                var match = _dinnerParser.Match(entry);

                var source = match.Groups[1].Value;

                if (!_diners.Contains(source)) {
                    _diners.Add(source);
                }
   
                var target = match.Groups[4].Value;

                if (!_diners.Contains(target))
                {
                    _diners.Add(target);
                }

                int modifier = match.Groups[2].Value == "gain"
                    ? Convert.ToInt32(match.Groups[3].Value)
                    : -Convert.ToInt32(match.Groups[3].Value);

                _modifiers.Add(new Tuple<int, int>(
                    _diners.FindIndex( a => a == source), 
                    _diners.FindIndex( a => a == target)), modifier);
            }

        }

        public int GetModifier(string a, string b)
        {
            return _modifiers[new Tuple<int, int>(Convert.ToInt32(a) - 1, Convert.ToInt32(b) - 1)];
        }

        public int GetModifier(int a, int b)
        {
            return _modifiers[new Tuple<int, int>(a, b)];
        }
    }

    public interface ICombinator {
        List<String> Combinate(int places);
    }

    public class Combinator : ICombinator
    {
        public List<string> Combinate(int places)
        {
            return Trawl(Enumerable.Range(1, places).ToList(), "", "", 0, new List<String>());
        }

        private List<string> Trawl(List<int> toVisit, string path, string lastPoint, int position, List<string> combinations)
        {
            foreach (var s in toVisit)
            {
                if (!path.Contains(s.ToString()))
                {
                    if (position < toVisit.Count - 1)
                    {
                        string newPath = path == string.Empty ? s.ToString() : path + "," + s.ToString();

                        Trawl(toVisit, newPath, s.ToString(), position + 1, combinations);
                    }
                    else
                    {
                        Debug.Print(path + "," + s);
                        combinations.Add(path + "," + s);
                    }
                }
            }
            return combinations;
        }
    }
}
