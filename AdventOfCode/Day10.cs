﻿using System.Collections.Generic;

namespace AdventOfCode
{
    public interface IStringSayer
    {
        string SayString(string stringToLookAt);
    }

    public class StringSayer : IStringSayer
    {
        public string SayString(string stringToLookAt)
        {
            var prev = stringToLookAt[0];
            int count = 0;
            
            var bits = new List<string>();
            foreach (var digit in stringToLookAt) {
                if (digit == prev)
                {
                    count = count + 1;
                }
                else
                {
                    bits.Add(count.ToString() + prev.ToString());
                    prev = digit;
                    count = 1;
                }
            }

            if (count != 0) {
                bits.Add(count.ToString() + prev.ToString());
            }

            return string.Join("", bits);
        }
    }
}
