﻿using System.Security.Cryptography;
using System.Text;

namespace AdventOfCode
{
    public interface IHashChecker {
        int MinValidHash(string secretKey, string hashPrefix);
    }

    public class HashChecker : IHashChecker {
        public int MinValidHash(string secretKey, string hashPrefix) {

            bool hashFound = false;
            int attempt = 0;
            MD5 md5Hash = MD5.Create();

            while (!hashFound)
            {   
                string hash = GetMd5Hash(md5Hash, secretKey + attempt.ToString());

                if (hash.StartsWith(hashPrefix)) {
                    break;
                }

                attempt++;
            }

            return attempt;
        }

        // copied from https://msdn.microsoft.com/en-us/library/system.security.cryptography.md5(v=vs.110).aspx
        private string GetMd5Hash(MD5 md5Hash, string input)
        {
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
    }
}
