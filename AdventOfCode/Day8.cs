﻿namespace AdventOfCode
{
    public interface IStringSizer {
        int GetMemorySize(string inputString);
    }

    public class StringSizer : IStringSizer
    {
        public int GetMemorySize(string inputString)
        {   
            inputString = inputString.Substring(1, inputString.Length - 2);
            inputString = "..." + inputString + "...";
            inputString = inputString.Replace("\\\"", "@@@@");
            inputString = inputString.Replace("\\\\", "####");
            inputString = inputString.Replace("\\x","!!!");
            return inputString.Length;
        }
    }
}
