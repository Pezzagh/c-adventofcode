﻿namespace AdventOfCode
{
    public interface IReindeerWorker {
        int Distance(int speedPerSeconds, int secondsActive, int secondsResting, int secondsElapsed);
    }

    public class ReindeerWorker : IReindeerWorker
    {
        public int Distance(int speedPerSecond, int secondsActive, int secondsResting, int secondsElapsed)
        {
            bool flying = true;
            int stage = secondsActive;
            int distance = 0;
            for (int timer = 1; timer <= secondsElapsed; timer += 1) {

                if (stage == 0) {
                    if (flying)
                    {
                        stage = secondsResting;
                    }
                    else
                    {
                        stage = secondsActive;
                    }
                    flying = !flying;
                }

                stage = stage - 1;
                if (flying) {
                    distance += speedPerSecond;
                }
            }

            return distance;
        }
    }
}
