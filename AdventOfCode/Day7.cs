﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace AdventOfCode
{
    public enum Wiring {
        Signal,
        And,
        Or,
        LShift,
        RShift,
        Not
    }

    public class CircuitWiring
    {
        public Wiring Instruction { get; set; }
        public string TargetWire { get; set; }
        public string SourceWire { get; set; }
        public bool SourceProcessed { get; set; }
        public string SourceWire2 { get; set; }
        public bool SourceProcessed2 { get; set; }
        public ushort Signal { get; set; }
        public int Shift { get; set; }
        public ushort WireSignal { get; set; }
        public bool SignalSent { get; set; }

        public override string ToString() {
            return WireSignal.ToString();
        }
    }

    public interface ICircuitWiringReader {
        CircuitWiring ParseWiring(string wiring);
    }

    public class CircuitWiringReader : ICircuitWiringReader
    {
        private readonly Regex lineParser = new Regex(@"(?<source>\w+) (?<command>AND|OR) (?<source2>\w+) -> (?<target>\w+)|(?<command>NOT) (?<source>\w+) -> (?<target>\w+)|(?<source>\w+) (?<command>LSHIFT|RSHIFT) (?<shift>\d+) -> (?<target>\w+)|(?<signal>\d+) -> (?<target>\w+)|(?<source>\w+) -> (?<target>\w+)");

        public CircuitWiring ParseWiring(string wiring)
        {
            GroupCollection groups = lineParser.Match(wiring).Groups;

            return new CircuitWiring {
                Signal = groups["signal"].Value == "" ? (ushort)0 : (ushort)(Convert.ToInt32(groups["signal"].Value)),
                Instruction = groups["command"].Value.GetWiring(),
                SourceWire = groups["source"].Value,
                SourceWire2 = groups["source2"].Value,
                TargetWire = groups["target"].Value,
                Shift = groups["shift"].Value == "" ? 0 : Convert.ToInt32(groups["shift"].Value)
            };
        }
    }

    public interface ICircuitBuilder {
        void WireUp(Dictionary<string, CircuitWiring> wires, CircuitWiring instruction);
    }

    public class CircuitBuilder : ICircuitBuilder
    {
        public void WireUp(Dictionary<string, CircuitWiring> wires, CircuitWiring instruction)
        {
            if (instruction.SignalSent) {
                Debug.Print(string.Format("XXXX signal already set on  {0}", instruction.TargetWire));
                return;
            }
            Debug.Print(string.Format("processing {0}", instruction.TargetWire));

            if (!instruction.SourceProcessed && !string.IsNullOrEmpty(instruction.SourceWire) && instruction.SourceWire != "1") {

                Debug.Print(string.Format("    -> stepping into L {0}", instruction.SourceWire));
                WireUp(wires, wires[instruction.SourceWire]);
                instruction.SourceProcessed = true;
            }

            if (!instruction.SourceProcessed2 && !string.IsNullOrEmpty(instruction.SourceWire2))
            {
                Debug.Print(string.Format("    -> stepping into R {0}", instruction.SourceWire2));
                WireUp(wires, wires[instruction.SourceWire2] );
                instruction.SourceProcessed2 = true;
            }

            ushort sourceSignal;
            switch (instruction.SourceWire) {
                case "":
                    sourceSignal = 0;
                    break;
                case "1":
                    sourceSignal = 1;
                    break;
                default:
                    sourceSignal = (ushort)wires[instruction.SourceWire].WireSignal;
                    break;

            }
      
            switch (instruction.Instruction) {
                case Wiring.Signal:
                    instruction.WireSignal = instruction.SourceWire != "" ? wires[instruction.SourceWire].WireSignal : instruction.Signal;
                    break;
                case Wiring.And:
                    instruction.WireSignal = (ushort)(sourceSignal & wires[instruction.SourceWire2].WireSignal);
                    break;
                case Wiring.Or:
                    instruction.WireSignal = (ushort)(sourceSignal | wires[instruction.SourceWire2].WireSignal);
                    break;
                case Wiring.Not:
                    instruction.WireSignal = (ushort)(~sourceSignal);
                    break;
                case Wiring.LShift:
                    instruction.WireSignal = (ushort)(sourceSignal << instruction.Shift);
                    break;
                case Wiring.RShift:
                    instruction.WireSignal = (ushort)(sourceSignal >> instruction.Shift);
                    break;
            }

            instruction.SignalSent = true;
            Debug.Print(string.Format("@@@@ Set {0} on {1}", instruction.WireSignal, instruction.TargetWire));

        }
    }
}
