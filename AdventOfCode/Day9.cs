﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace AdventOfCode
{
    public interface IRouteLoader {

        void AddRoute(string route);

        List<string> Destinations { get; }

        int GetDistance(string startDestination, string endDestination);

        int GetShortestDistance();

        int GetLongestDistance();
    }

    public class RouteLoader : IRouteLoader
    {
        private Regex _routeParser = new Regex(@"(\w+) to (\w+) = (\d+)");

        private List<string> _destinations;

        private Dictionary<string, int> _distances;

        public RouteLoader() {
            _destinations = new List<string>();
            _distances = new Dictionary<string, int>();
        }

        public void AddRoute(string route)
        {
            var match = _routeParser.Match(route);

            string depart = match.Groups[1].Value;
            string dest = match.Groups[2].Value;
            int distance = Convert.ToInt32(match.Groups[3].Value);

            if (!_destinations.Contains(depart)) {
                _destinations.Add(depart);
            }

            if (!_destinations.Contains(dest)) {
                _destinations.Add(dest);
            }

            if (string.Compare(depart, dest, true) < 0) {
                _distances.Add(depart + dest, distance);
            }

            if (string.Compare(depart, dest, true) > 0)
            {
                _distances.Add(dest + depart, distance);
            }
        }

        public List<string> Destinations
        {
            get {
                return _destinations;
            }
        }

        public int GetDistance(string startDestination, string endDestination)
        {
            if (string.Compare(startDestination, endDestination, true) < 0)
            {
                return _distances[startDestination + endDestination];
            }

            if (string.Compare(startDestination, endDestination, true) > 0)
            {
                return _distances[endDestination + startDestination];
            }

            return 0;
        }

        public int GetShortestDistance() {
            return Trawl(_destinations, "","", 0, Int32.MaxValue, false);
        }

        public int GetLongestDistance()
        {
            return Trawl(_destinations, "", "", 0, 0, true);
        }

        private int Trawl(List<string> toVisit, string path, string lastPoint, int position, int shortest, bool getHighest) {
            var dist = shortest;

            foreach (var s in toVisit)
            {
                if (!path.Contains(s))
                {
                    if (position < toVisit.Count - 1)
                    {
                        string newPath = path == string.Empty ? s : path + "," + s;

                        dist = Trawl(toVisit, newPath, s, position + 1, dist, getHighest);
                    }
                    else {
                        path = path + "," + s;
                        var places = path.Split(',');

                        var last = 0;
                        for (int i = 0; i < places.Length; i += 1) {
                            if (i > 0) {

                                last += GetDistance(places[i], places[i - 1]);
                            }
                        }

                        if (getHighest)
                        {
                            if (last > dist)
                            {
                                dist = last;
                            }
                            Debug.Print(String.Format("Routed {0}, distance was {1}, longest overall is {2}", string.Join(" -> ", places), last, dist));
                        }
                        else
                        {
                            if (last < dist)
                            {
                                dist = last;
                            }
                            Debug.Print(String.Format("Routed {0}, distance was {1}, shortest overall is {2}", string.Join(" -> ", places), last, dist));
                        }
                        
                    }
                }
            }

            return dist;
        }
    }
}
