﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode
{
    public interface IStringSplitter {
        IList<string> GetTokens(string stringToSplit, int splitSize, bool removeConsecutiveDupes = false);
    }

    public class Splitter : IStringSplitter {
        public IList<string> GetTokens(string stringToSplit, int splitSize, bool removeConsecutiveDupes = false)
        {
            var retval = new List<string>();

            var pos = 0;
            var prev = string.Empty;
            for (int loop = splitSize; loop <= stringToSplit.Length; loop = loop + 1)
            {
                var nextToken = stringToSplit.Substring(pos, splitSize);

                if (!(removeConsecutiveDupes && nextToken == prev))
                {
                    retval.Add(nextToken);
                    prev = nextToken;
                }
                else {
                    prev = string.Empty;
                }

                
                pos = pos + 1;
            }

            return retval;
        }
    }

    public interface INicenessChecker {
        bool IsStringNice(string check);
        bool IsStringReallyNice(string check);
    }

    public class NicenessChecker : INicenessChecker
    {
        private static string letters = "abcdefghijklmnopqrstuvwxyz";
        private static string vowels = "aeiou";
        private readonly IStringSplitter _stringSplitter;

        public NicenessChecker(IStringSplitter stringSplitter) {
            _stringSplitter = stringSplitter;
        }
                
        public bool IsStringNice(string check)
        {
            if (check.Contains("ab") || check.Contains("cd") || check.Contains("pq") || check.Contains("xy"))
            {
                return false;
            }

            var vowelCount = 0;

            foreach (var s in check)
            {
                if (vowels.Contains(s.ToString()))
                {
                    vowelCount = vowelCount + 1;
                }
            }

            if (vowelCount < 3)
            {
                return false;
            }

            foreach (var s in letters)
            {
                if (check.Contains(s.ToString() + s.ToString()))
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsStringReallyNice(string check)
        {
            bool hasPalindrome = false;
            foreach (var s in _stringSplitter.GetTokens(check, 3)){
                if (s[0] == s[2]) {
                    hasPalindrome = true;
                    break;
                }
            }

            if (!hasPalindrome) {
                return false;
            }

            var prev = string.Empty;

            var sortedPairTokens = _stringSplitter.GetTokens(check, 2, true).OrderBy(f => f).ToList();
            foreach (var s in sortedPairTokens) {
                if (s == prev) {
                    return true;
                }
                prev = s;
            }

            return false;
        }
    }
}
