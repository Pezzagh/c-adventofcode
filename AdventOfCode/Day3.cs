﻿using System;
using System.Collections.Generic;

namespace AdventOfCode
{
    public interface IPresentLocator {
        int GetDropOffCount(string directions, int agents);
    }

    public class Locator : IPresentLocator {

        public int GetDropOffCount(string directions, int agents) {
            var agentList = new Tuple<int, int>[agents];

            for (var i = 0; i < agents; i = i + 1) {
                agentList[i] = new Tuple<int, int>(0, 0);
            }

            var houses = new Dictionary<Tuple<int, int>, int>();

            int agentIndex = 0;

            // first house gets a present per agent
            houses.Add(agentList[0], agents);
          
            foreach (var s in directions)
            {
                switch (s)
                {
                    case '>':
                        agentList[agentIndex] = new Tuple<int, int>(agentList[agentIndex].Item1 + 1, agentList[agentIndex].Item2);
                        break;
                    case '<':
                        agentList[agentIndex] = new Tuple<int, int>(agentList[agentIndex].Item1 - 1, agentList[agentIndex].Item2);
                        break;
                    case '^':
                        agentList[agentIndex] = new Tuple<int, int>(agentList[agentIndex].Item1, agentList[agentIndex].Item2 + 1);
                        break;
                    case 'v':
                        agentList[agentIndex] = new Tuple<int, int>(agentList[agentIndex].Item1, agentList[agentIndex].Item2 - 1);
                        break;
                }

                if (houses.ContainsKey(agentList[agentIndex]))
                {
                    houses[agentList[agentIndex]]++;
                }
                else
                {
                    houses.Add(new Tuple<int, int>(agentList[agentIndex].Item1, agentList[agentIndex].Item2), 1);
                }

                agentIndex = agentIndex + 1;
                if (agentIndex == agents) {
                    agentIndex = 0;
                }
            }

            return houses.Count;
        }
    }
}
