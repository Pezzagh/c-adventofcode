﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Tests
{
    [TestClass]
    public class Day14
    {
        private Regex _reindeerParser = new Regex(@"(\w+) can fly (\d+) km/s for (\d+) seconds, but then must rest for (\d+) seconds.");

        [TestMethod]
        public void TestReindeer() {
            IReindeerWorker reindeerWorker = new ReindeerWorker();
            Assert.AreEqual(1120, reindeerWorker.Distance(14, 10, 127, 1000));
            Assert.AreEqual(1056, reindeerWorker.Distance(16, 11, 162, 1000));
        }

        [TestMethod]
        public void Challenge1() {
            IReindeerWorker reindeerWorker = new ReindeerWorker();
            var dataFile = File.ReadLines(@"D:\bitbucket\AdventOfCode\DataFiles\Day14.txt");

            var bestDistance = 0;
            foreach (var reindeerRecord in dataFile) {
                var match = _reindeerParser.Match(reindeerRecord);

                var reindeerDistance =
                        reindeerWorker.Distance(
                            Convert.ToInt32(match.Groups[2].Value),
                            Convert.ToInt32(match.Groups[3].Value),
                            Convert.ToInt32(match.Groups[4].Value),
                            2503);

                if (reindeerDistance > bestDistance) {
                    bestDistance = reindeerDistance;
                }
            }

            Assert.AreEqual(2640, bestDistance);
        }

        [TestMethod]
        public void Challenge2()
        {
            IReindeerWorker reindeerWorker = new ReindeerWorker();
            var dataFile = File.ReadLines(@"D:\bitbucket\AdventOfCode\DataFiles\Day14.txt");

            var bestDistance = 0;

            var reindeers = new Dictionary<string, int>();

            foreach (var reindeerRecord in dataFile)
            {
                var match = _reindeerParser.Match(reindeerRecord);
                reindeers.Add(match.Groups[1].Value, 0);
            }

            foreach (var i in Enumerable.Range(1, 2503))
            {
                var secondScore = new Dictionary<string, int>();

                foreach (var reindeerRecord in dataFile)
                {
                    var match = _reindeerParser.Match(reindeerRecord);

                    secondScore.Add(
                        match.Groups[1].Value,
                        reindeerWorker.Distance(
                                Convert.ToInt32(match.Groups[2].Value),
                                Convert.ToInt32(match.Groups[3].Value),
                                Convert.ToInt32(match.Groups[4].Value),
                                i));
                }

                var winningScore = 0;
                foreach (var score in secondScore) {
                    if (score.Value > winningScore) {
                        winningScore = score.Value;

                    }
                }

                foreach (var score in secondScore)
                {
                    if (score.Value == winningScore)
                    {
                        reindeers[score.Key] += 1;

                    }
                }

            }

            var overallScore = 0;
            foreach (var reindeer in reindeers) {
                if (reindeer.Value > overallScore) {
                    overallScore = reindeer.Value;
                }
            }

            Assert.AreEqual(1102, overallScore);
        }
    }
}
