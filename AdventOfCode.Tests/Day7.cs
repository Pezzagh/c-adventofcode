﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;

namespace AdventOfCode.Tests
{
    [TestClass]
    public class Day7
    {
        [TestMethod]
        public void TestWiringParser()
        {
            ICircuitWiringReader reader = new CircuitWiringReader();

            var wiring = "123 -> x";

            var output = reader.ParseWiring(wiring);
            Assert.AreEqual(123, output.Signal);
            Assert.AreEqual(Wiring.Signal, output.Instruction);
            Assert.AreEqual("x", output.TargetWire);

            wiring = "x AND y -> d";
            output = reader.ParseWiring(wiring);
            Assert.AreEqual(0, output.Signal);
            Assert.AreEqual(Wiring.And, output.Instruction);
            Assert.AreEqual("d", output.TargetWire);
        }

        [TestMethod]
        public void TestCircuit()
        {
            ICircuitWiringReader reader = new CircuitWiringReader();
            ICircuitBuilder builder = new CircuitBuilder();

            var simpleCircuit = File.ReadLines(@"D:\bitbucket\AdventOfCode\DataFiles\Day7.txt");

            var finalCircuit = new Dictionary<string, CircuitWiring>();

            foreach (var s in simpleCircuit)
            {
                var circuitWiring = reader.ParseWiring(s);
                finalCircuit.Add(circuitWiring.TargetWire, circuitWiring);
            }

            finalCircuit["b"].WireSignal = 46065;
            finalCircuit["b"].SignalSent = true;

            foreach (var wire in finalCircuit)
            {
                builder.WireUp(finalCircuit, wire.Value);
            }

            Assert.AreEqual(72, finalCircuit["a"].WireSignal);
        }
    }
}
