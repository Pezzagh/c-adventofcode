﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;

namespace AdventOfCode.Tests
{
    [TestClass]
    public class Day10
    {
        [TestMethod]
        public void TestCases()
        {
            IStringSayer sayer = new StringSayer();

            Assert.AreEqual("11", sayer.SayString("1"));
            Assert.AreEqual("21", sayer.SayString("11"));
            Assert.AreEqual("1211", sayer.SayString("21"));
            Assert.AreEqual("111221", sayer.SayString("1211"));
            Assert.AreEqual("312211", sayer.SayString("111221"));

        }

        [TestMethod]
        public void Challenge_1()
        {
            IStringSayer sayer = new StringSayer();

            var startString = "1321131112";

            int count = 1;

            for (var i = 0; i < 50; i += 1) {
                startString = sayer.SayString(startString);
                Debug.Print(String.Format("{0} : {1}", count, startString.Length));
                count += 1;
            }

            Assert.AreEqual(123, startString.Length);
        }
    }
}
