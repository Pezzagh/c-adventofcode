﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace AdventOfCode.Tests
{
    [TestClass]
    public class Day5
    {
        [TestMethod]
        public void Splitter()
        {
            IStringSplitter splitter = new Splitter();

            var testString = ("abcdefg");
            var firstCut = splitter.GetTokens(testString, 1);
            Assert.AreEqual(7, firstCut.Count);

            firstCut = splitter.GetTokens(testString, 2);
            Assert.AreEqual(6, firstCut.Count);

            firstCut = splitter.GetTokens(testString, 3);
            Assert.AreEqual(5, firstCut.Count);
            Assert.AreEqual("abc", firstCut[0]);
            Assert.AreEqual("efg", firstCut[4]);

            Assert.AreEqual(2, splitter.GetTokens("aaa", 2).Count);
            Assert.AreEqual(3, splitter.GetTokens("xyxy", 2, true).Count);
            Assert.AreEqual(1, splitter.GetTokens("aaa", 2, true).Count);
            Assert.AreEqual(2, splitter.GetTokens("aaaa", 2, true).Count);
            Assert.AreEqual(1, splitter.GetTokens("aaaa", 3, true).Count);
            Assert.AreEqual(4, splitter.GetTokens("aaabaa", 2, true).Count);
        }

        [TestMethod]
        public void Test_1()
        {
            INicenessChecker checker = new NicenessChecker(new Splitter());
            Assert.AreEqual(true, checker.IsStringNice("ugknbfddgicrmopn"));
            Assert.AreEqual(true, checker.IsStringNice("aaa"));
            Assert.AreEqual(false, checker.IsStringNice("jchzalrnumimnmhp"));
            Assert.AreEqual(false, checker.IsStringNice("haegwjzuvuyypxyu"));
            Assert.AreEqual(false, checker.IsStringNice("dvszwmarrgswjxmb"));
        }

        [TestMethod]
        public void Test_2()
        {
            INicenessChecker checker = new NicenessChecker(new Splitter());
            Assert.AreEqual(true, checker.IsStringReallyNice("qjhvhtzxzqqjkmpb"));
            Assert.AreEqual(true, checker.IsStringReallyNice("xxyxx"));
            Assert.AreEqual(false, checker.IsStringReallyNice("uurcxstgmygtbstg"));
            Assert.AreEqual(false, checker.IsStringReallyNice("ieodomkazucvgmuy"));
            Assert.AreEqual(true, checker.IsStringReallyNice("qjhvhtzzzqqjkmpb"));
        }

        [TestMethod]
        public void ChallengeChecker_1()
        {
            INicenessChecker checker = new NicenessChecker(new Splitter());
            var checks = File.ReadLines(@"D:\bitbucket\AdventOfCode\DataFiles\Day5.txt");
            var niceCount = 0;
            foreach (var s in checks)
            {
                if (checker.IsStringNice(s))
                {
                    niceCount = niceCount + 1;
                }
            }
            Assert.AreEqual(238, niceCount);
        }

        [TestMethod]
        public void ChallengeChecker_2()
        {
            INicenessChecker checker = new NicenessChecker(new Splitter());
            var checks = File.ReadLines(@"D:\bitbucket\AdventOfCode\DataFiles\Day5.txt");
            var niceCount = 0;
            foreach (var s in checks)
            {
                if (checker.IsStringReallyNice(s))
                {
                    niceCount = niceCount + 1;
                }
            }
            Assert.AreEqual(69, niceCount);
        }
    }
}
