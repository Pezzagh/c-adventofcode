﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace AdventOfCode.Tests
{
    [TestClass]
    public class Day3
    {
        [TestMethod]
        public void Tests()
        {
            IPresentLocator locator = new Locator();
            Assert.AreEqual(2, locator.GetDropOffCount("^v", 1));
            Assert.AreEqual(4, locator.GetDropOffCount("^>v<", 1));
            Assert.AreEqual(2, locator.GetDropOffCount("^v^v^v^v^v", 1));
            Assert.AreEqual(2565, locator.GetDropOffCount(File.ReadAllText(@"D:\Bitbucket\ConsoleApplication1\ConsoleApplication1\Day3.txt"), 1));
            Assert.AreEqual(3, locator.GetDropOffCount("^v", 2));
            Assert.AreEqual(3, locator.GetDropOffCount("^>v<", 2));
            Assert.AreEqual(11, locator.GetDropOffCount("^v^v^v^v^v", 2));
            Assert.AreEqual(2639, locator.GetDropOffCount(File.ReadAllText(@"D:\Bitbucket\ConsoleApplication1\ConsoleApplication1\Day3.txt"), 2));
        }
    }
}
