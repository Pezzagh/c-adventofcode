﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace AdventOfCode.Tests
{
    [TestClass]
    public class Day8
    {
        [TestMethod]
        public void TestWiringParser()
        {
            IStringSizer sizer = new StringSizer();
            Assert.AreEqual(0, sizer.GetMemorySize(@""""""));
            Assert.AreEqual(3, sizer.GetMemorySize(@"""abc"""));
            Assert.AreEqual(7, sizer.GetMemorySize(@"""aaa\""aaa"""));
            Assert.AreEqual(1, sizer.GetMemorySize(@"""\x27"""));
        }

        [TestMethod]
        public void Challenge()
        {
            IStringSizer sizer = new StringSizer();
            var codeLines = File.ReadLines(@"D:\bitbucket\AdventOfCode\DataFiles\Day8.txt");

            int totCode = 0;
            int totMem = 0;
            foreach (var codeLine in codeLines)
            {
                totCode += codeLine.Trim().Length;
                totMem += sizer.GetMemorySize(codeLine.Trim());
            }
            // not 2252
            Assert.AreEqual(0, totMem - totCode);
        }
    }
}