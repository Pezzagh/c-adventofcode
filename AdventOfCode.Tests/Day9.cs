﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;

namespace AdventOfCode.Tests
{
    [TestClass]
    public class Day9
    {
        [TestMethod]
        public void TestLoader() {
            var routes = new List<string>
            {
                "London to Dublin = 464",
                "London to Belfast = 518",
                "Dublin to Belfast = 141"
            };

            IRouteLoader loader = new RouteLoader();

            foreach (var route in routes) {
                loader.AddRoute(route);
            }

            Assert.AreEqual(3, loader.Destinations.Count);
            Assert.AreEqual(464, loader.GetDistance("London", "Dublin"));
            Assert.AreEqual(518, loader.GetDistance("London", "Belfast"));
            Assert.AreEqual(141, loader.GetDistance("Belfast", "Dublin"));
        }

        [TestMethod]
        public void Challenge_Test()
        {
            var routes = new List<string>
            {
                "London to Dublin = 464",
                "London to Belfast = 518",
                "Dublin to Belfast = 141"
            };

            IRouteLoader loader = new RouteLoader();

            foreach (var route in routes)
            {
                loader.AddRoute(route);
            }

            Assert.AreEqual(605, loader.GetShortestDistance());
        }

        [TestMethod]
        public void Challenge_1() {
            var routes = File.ReadLines(@"D:\bitbucket\AdventOfCode\DataFiles\Day9.txt");

            IRouteLoader loader = new RouteLoader();

            foreach (var route in routes)
            {
                loader.AddRoute(route);
            }

            Assert.AreEqual(251, loader.GetShortestDistance()); 
        }

        [TestMethod]
        public void Challenge_2()
        {
            var routes = File.ReadLines(@"D:\bitbucket\AdventOfCode\DataFiles\Day9.txt");

            IRouteLoader loader = new RouteLoader();

            foreach (var route in routes)
            {
                loader.AddRoute(route);
            }

            Assert.AreEqual(898, loader.GetLongestDistance());
        }
    }
}
