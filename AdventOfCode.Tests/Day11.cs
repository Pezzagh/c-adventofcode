﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AdventOfCode.Tests
{
    [TestClass]
    public class Day11
    {
        [TestMethod]
        public void TestIncrementor() {
            IPasswordIncrementer passwordIncrementor = new PasswordIncrementor();
            Assert.AreEqual("ab", passwordIncrementor.Increment("aa"));
            Assert.AreEqual("ba", passwordIncrementor.Increment("az"));
            Assert.AreEqual("baaaa", passwordIncrementor.Increment("azzzz"));
            Assert.AreEqual("aaaaa", passwordIncrementor.Increment("zzzz"));

        }
        [TestMethod]
        public void TestCases() {
            IPasswordChecker checker = new PasswordChecker(new Splitter());

            Assert.AreEqual(false, checker.ValidPassword("hijklmmn"));
            Assert.AreEqual(false, checker.ValidPassword("abbceffg"));
            Assert.AreEqual(false, checker.ValidPassword("abbcegjk"));
            Assert.AreEqual(true, checker.ValidPassword("abcdffaa"));
            Assert.AreEqual(true, checker.ValidPassword("ghjaabcc"));
        }

        [TestMethod]
        public void Challenge()
        {
            IPasswordChecker checker = new PasswordChecker(new Splitter());
            IPasswordIncrementer incrementor = new PasswordIncrementor();

            var password = "hxbxwxba";

            while (true) {
                password = incrementor.Increment(password);

                if (checker.ValidPassword(password)) {
                    break;
                }
            }

            Assert.AreEqual("hxbxxyzz", password);
        }

        [TestMethod]
        public void Challenge_2()
        {
            IPasswordChecker checker = new PasswordChecker(new Splitter());
            IPasswordIncrementer incrementor = new PasswordIncrementor();

            var password = "hxbxxyzz";

            while (true)
            {
                password = incrementor.Increment(password);

                if (checker.ValidPassword(password))
                {
                    break;
                }
            }

            Assert.AreEqual("hxcaabcc", password);
        }
    }
}
