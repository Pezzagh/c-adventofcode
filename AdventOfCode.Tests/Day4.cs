﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AdventOfCode.Tests
{
    [TestClass]
    public class Day4
    {
        [TestMethod]
        public void Tests()
        {
            IHashChecker checker = new HashChecker();
            Assert.AreEqual(609043, checker.MinValidHash("abcdef", "00000"));
            Assert.AreEqual(1048970, checker.MinValidHash("pqrstuv", "00000"));
            Assert.AreEqual(346386, checker.MinValidHash("iwrupvqb", "00000"));
            Assert.AreEqual(9958218, checker.MinValidHash("iwrupvqb", "000000"));
        }
    }

}
