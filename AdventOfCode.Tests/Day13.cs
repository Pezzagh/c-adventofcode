﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode.Tests
{
    [TestClass]
    public class Day13
    {
        [TestMethod]
        public void TestCombinator() {
            ICombinator combinator = new Combinator();
            Assert.AreEqual(40320, combinator.Combinate(8).Count);
        }

        [TestMethod]
        public void TestLoader()
        {
            IDinnerLoader dinnerLoader = new DinnerLoader();

            dinnerLoader.AddDinerEntries(File.ReadLines(@"D:\bitbucket\AdventOfCode\DataFiles\Day13Test.txt").ToList());

            Assert.AreEqual(54, dinnerLoader.GetModifier(0, 1));
            Assert.AreEqual(-79, dinnerLoader.GetModifier(0, 2));
        }

        [TestMethod]
        public void Challenge_1() {
            IDinnerLoader dinnerLoader = new DinnerLoader();

            dinnerLoader.AddDinerEntries(File.ReadLines(@"D:\bitbucket\AdventOfCode\DataFiles\Day13.txt").ToList());

            ICombinator combinator = new Combinator();

            var maxHappiness = 0;
            foreach (var s in combinator.Combinate(9)) {
                var positions = s.Split(',');

                var modifier = 0;

                modifier += dinnerLoader.GetModifier(positions[0], positions[8]);
                modifier += dinnerLoader.GetModifier(positions[0], positions[1]);
                modifier += dinnerLoader.GetModifier(positions[1], positions[0]);
                modifier += dinnerLoader.GetModifier(positions[1], positions[2]);
                modifier += dinnerLoader.GetModifier(positions[2], positions[1]);
                modifier += dinnerLoader.GetModifier(positions[2], positions[3]);
                modifier += dinnerLoader.GetModifier(positions[3], positions[2]);
                modifier += dinnerLoader.GetModifier(positions[3], positions[4]);
                modifier += dinnerLoader.GetModifier(positions[4], positions[3]);
                modifier += dinnerLoader.GetModifier(positions[4], positions[5]);
                modifier += dinnerLoader.GetModifier(positions[5], positions[4]);
                modifier += dinnerLoader.GetModifier(positions[5], positions[6]);
                modifier += dinnerLoader.GetModifier(positions[6], positions[5]);
                modifier += dinnerLoader.GetModifier(positions[6], positions[7]);
                modifier += dinnerLoader.GetModifier(positions[7], positions[6]);
                modifier += dinnerLoader.GetModifier(positions[7], positions[8]);
                modifier += dinnerLoader.GetModifier(positions[8], positions[7]);
                modifier += dinnerLoader.GetModifier(positions[8], positions[0]);

                if (modifier > maxHappiness) {
                    maxHappiness = modifier;
                }
                Debug.Print(string.Format("{0} = > {1}.  Max so far is {2}", s, modifier, maxHappiness));
            }

            Assert.AreEqual(725, maxHappiness);
        }
    }
}

