﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode.Tests
{
    [TestClass]
    public class Day12
    {
        [TestMethod]
        public void TestCases()
        {
            IObjectAggregator objectAggregator = new ObjectAggregator();
            Assert.AreEqual(4, objectAggregator.Aggregate("{ \"a\": 4 }"));
            Assert.AreEqual(6, objectAggregator.Aggregate("[1,2,3]"));
            Assert.AreEqual(3, objectAggregator.Aggregate("[[[3]]]"));
            Assert.AreEqual(3, objectAggregator.Aggregate("{\"a\":{\"b\":4},\"c\":-1}"));
            Assert.AreEqual(0, objectAggregator.Aggregate("{}"));
            Assert.AreEqual(0, objectAggregator.Aggregate("[]"));
            Assert.AreEqual(0, objectAggregator.Aggregate("{\"a\":[-1,1]}"));
            Assert.AreEqual(0, objectAggregator.Aggregate("[-1,{\"a\":1}]"));
        }

        [TestMethod]
        public void Challenge_1()
        {

            Assert.AreEqual(65402,
                    new ObjectAggregator().Aggregate(
                            File.ReadAllText(@"D:\bitbucket\AdventOfCode\DataFiles\Day12.txt")));
        }
    }
}
