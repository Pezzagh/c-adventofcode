﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace AdventOfCode.Tests
{
    [TestClass]
    public class Day6
    {
        [TestMethod]
        public void TestCanCountLights()
        {
            ILightSwitcher lightSwitcher = new LightSwitcher();

            bool[,] testLights = new bool[5, 5];

            Assert.AreEqual(0, lightSwitcher.CountLights(testLights));
            lightSwitcher.SwitchLights(testLights, Action.On, new Point { X = 0, Y = 0 }, new Point { X = 0, Y = 0 });

            Assert.AreEqual(1, lightSwitcher.CountLights(testLights));
            lightSwitcher.SwitchLights(testLights, Action.On, new Point { X = 0, Y = 0 }, new Point { X = 4, Y = 4 });

            Assert.AreEqual(25, lightSwitcher.CountLights(testLights));
            lightSwitcher.SwitchLights(testLights, Action.Off, new Point { X = 2, Y = 2 }, new Point { X = 2, Y = 2 });

            Assert.AreEqual(24, lightSwitcher.CountLights(testLights));
            lightSwitcher.SwitchLights(testLights, Action.Toggle, new Point { X = 2, Y = 0 }, new Point { X = 2, Y = 4 });

            Assert.AreEqual(21, lightSwitcher.CountLights(testLights));
        }

        [TestMethod]
        public void Challenge()
        {
            var instructions = File.ReadLines(@"D:\bitbucket\AdventOfCode\DataFiles\Day6.txt");

            IInstructionParser parser = new InstructionParser();
            ILightSwitcher lightSwitcher = new LightSwitcher();

            var nationalGrid = new bool[1000, 1000];

            foreach (var instruction in instructions)
            {

                var parsedIns = parser.ParseText(instruction);

                lightSwitcher.SwitchLights(nationalGrid, parsedIns.Action, parsedIns.TopLeft, parsedIns.BottomRight);
            }

            Assert.AreEqual(543903, lightSwitcher.CountLights(nationalGrid));
        }

        [TestMethod]
        public void Challenge_2()
        {
            var instructions = File.ReadLines(@"D:\bitbucket\AdventOfCode\DataFiles\Day6.txt");

            IInstructionParser parser = new InstructionParser();
            ILightSwitcher lightSwitcher = new LightSwitcher();

            var nationalGrid = new int[1000, 1000];

            foreach (var instruction in instructions)
            {

                var parsedIns = parser.ParseText(instruction);

                lightSwitcher.PhaseLights(nationalGrid, parsedIns.Action, parsedIns.TopLeft, parsedIns.BottomRight);
            }

            Assert.AreEqual(14687245, lightSwitcher.LumenCount(nationalGrid));
        }
    }
}
