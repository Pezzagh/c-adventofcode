﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode.Tests
{
    [TestClass]
    public class Day15
    {
        [TestMethod]
        public void TestCase() {
            ICookieLoader cookierLoader = new CookieLoader();

            cookierLoader.AddIngredient("Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8");
            cookierLoader.AddIngredient("Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3");

            Assert.AreEqual(62842880, cookierLoader.TestCookie(new Dictionary <string, int> {
                { "Butterscotch", 44 },
                { "Cinnamon", 56 }
            }));

        }

        [TestMethod]
        public void TestFormulation() {
            ICookieLoader cookierLoader = new CookieLoader();

            cookierLoader.AddIngredient("Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8");
            cookierLoader.AddIngredient("Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3");


            int bestSoFar = 0;
            
            for (int i = 1; i <= 100; i = i + 1) {

                int result = cookierLoader.TestCookie(new Dictionary<string, int> {
                    { "Butterscotch", i },
                    { "Cinnamon", 100 - i }
                });

                if (result > bestSoFar) {
                    bestSoFar = result;
                }
            }

            Assert.AreEqual(62842880, bestSoFar);
        }
    }
}
