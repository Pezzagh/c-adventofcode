﻿module HackerRank

// https://www.hackerrank.com/challenges/fp-list-replication
open System

let rec writeLines loopCount = 
    try
        let inVal = Console.ReadLine() |> int
        for i in [1..loopCount] do    
            printfn "%d" inVal
        writeLines loopCount    
    with
        | :? System.FormatException -> ()


//[<EntryPoint>]
let main2 argv = 
    let repeats = Console.ReadLine() |> int
    writeLines repeats
    0

//https://www.hackerrank.com/challenges/fp-filter-array
open System

let rec DoSmallest minCount = 
    try
        let inVal = Console.ReadLine() |> int
      
        match inVal with
        | var1 when var1 < minCount ->  printfn "%d" var1;  
        | _ -> ()
        DoSmallest minCount
    with
        | :? System.FormatException -> ()


let main3 argv = 
    let min = Console.ReadLine() |> int
    DoSmallest min
    0

 //https://www.hackerrank.com/challenges/fp-filter-positions-in-a-list

 // https://www.hackerrank.com/challenges/fp-hello-world-n-times
open System

let main argv = 
    let repeats = Console.ReadLine() |> int
    for i in [1..repeats] do
        printfn "Hello World"
    0 



