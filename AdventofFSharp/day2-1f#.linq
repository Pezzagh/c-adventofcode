<Query Kind="FSharpExpression" />

let TotalArea l w h =
	(2 * l * w) + (2 * w * h) + (2 * h * l)

let AreaWithSlack l w h =
	let sortedLengths = List.sort [l; w; h]
	sortedLengths.[0] * sortedLengths.[1] + TotalArea l w h


let readLines filePath = System.IO.File.ReadLines("day2.txt");

let box = AreaWithSlack 1 1 10 
printfn "%d" box

	
	