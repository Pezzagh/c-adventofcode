﻿let square x = x * x

let sumOfSquares n = 
    [0..n] 
    |> List.map square 
    |> List.sum

sumOfSquares 100

let s2 = square 2
let s5 = square 5