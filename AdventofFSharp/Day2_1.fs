﻿module Day2_1

open System

let TotalArea l w h =
    (2 * l * w) + (2 * w * h) + (2 * h * l)

let AreaWithSlack l w h =
    let sortedLengths = List.sort [l; w; h]
    sortedLengths.[0] * sortedLengths.[1] + TotalArea l w h

let PerimeterRibbon l w h = 
    let sortedLengths = List.sort [l; w; h]
    (sortedLengths.[0] * 2) + (sortedLengths.[1] * 2) 

let GetInput = 
    System.IO.File.ReadLines(@"D:\bitbucket\Tutorial1\Tutorial1\day2.txt")
        |> Seq.map (fun line -> 
                        line.Split([|'x'|]))

let PaperNeeded = 
    let sizes = 
        GetInput
        |> Seq.map (fun line -> 
                            let len = int line.[0]
                            let width = int line.[1]
                            let height = int line.[2]
                            AreaWithSlack len width height) 
    let mutable d = 0
    for x in sizes do
        d <- d + x 
    d                               

let PermiterRibbonNeeded = 
    let perimeterLengths = 
        GetInput
        |> Seq.map (fun line -> 
                            let len = int line.[0]
                            let width = int line.[1]
                            let height = int line.[2]
                            PerimeterRibbon len width height) 
                              
    let mutable d = 0
    for x in perimeterLengths do
        d <- d + x 
    d    

let BowRibbonNeeded = 
    let cubicVolumes = 
        GetInput
        |> Seq.map (fun line -> 
                            int line.[0] * int line.[1] * int line.[2])   
    let mutable d = 0
    for x in cubicVolumes do
        d <- d + x 
    d    

//[<EntryPoint>]
let main argv = 
    printf "Paper need is %d, Bow Ribbon need is %d, Perimeter ribbon needed is %d, a total of %d ribbon in total." PaperNeeded BowRibbonNeeded PermiterRibbonNeeded (BowRibbonNeeded + PermiterRibbonNeeded)
    0