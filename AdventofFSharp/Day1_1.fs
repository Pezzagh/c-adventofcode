﻿module day1_1

let floor steps = 
    Seq.fold (fun level instruction -> 
        match instruction with 
        | '(' -> level + 1 
        | _ -> level - 1) 0 steps

printf "Santa will end up on floor %i\n" (floor "(((((")

        